package State

abstract class State(val player : Player){
    abstract fun onLock() : String
    abstract fun onPlay() : String
    abstract fun onNext() : String
    abstract fun onPrevious() : String
}


class LockedState : State{
    constructor(player : Player) : super(player){
        player.setPlaying(false)
    }

    override fun onLock(): String {
        if (player.isPlaying()) {
            player.changeState(ReadyState(player))
            return "Stop playing"
        } else {
            return "Locked"
        }
    }

    override fun onPlay(): String {
        player.changeState(ReadyState(player))
        return "Ready"
    }

    override fun onNext(): String = "Locked"

    override fun onPrevious(): String = "Locked"
}

class ReadyState : State{
    constructor(player : Player) : super(player)

    override fun onLock(): String {
        player.changeState(LockedState(player))
        return "Locked"
    }

    override fun onPlay(): String {
        val action : String = player.startPlayback()
        player.changeState(PlayingState(player))
        return action
    }

    override fun onNext(): String  = "Locked"

    override fun onPrevious(): String = "Locked"
}

class PlayingState : State{
    constructor(player : Player) : super(player)

    override fun onLock(): String {
        player.changeState(LockedState(player))
        player.setCurrentTrackAfterStop()
        return "Stop playing"
    }

    override fun onPlay(): String {
        player.changeState(ReadyState(player))
        return "Paused"
    }

    override fun onNext(): String = player.nextTrack()

    override fun onPrevious(): String = player.previousTrack()

}


class Player{
    private var state : State
    private var playList = ArrayList<String>()
    private var playing: Boolean = false
    private var currentTrack: Int = 0

    init {
        this.state = ReadyState(this)
        setPlaying(true)
        for (i in 1..12){
            playList.add("Track ${i}")
        }
    }

    fun changeState(state : State) {
        this.state = state
    }

    fun getState() : State = state

    fun setPlaying(playing : Boolean){
        this.playing = playing
    }

    fun isPlaying() : Boolean = playing

    fun startPlayback():String = "Playing ${playList.get(currentTrack)}"

    fun nextTrack():String {
        currentTrack+=1
        if (currentTrack > playList.size - 1){
            currentTrack = 0
        }
        return "Playing ${playList.get(currentTrack)}"
    }

    fun previousTrack() : String{
        currentTrack-=1
        if (currentTrack < 0){
            currentTrack = playList.size - 1
        }
        return "Playing ${playList.get(currentTrack)}"
    }

    fun setCurrentTrackAfterStop(){
        this.currentTrack = 0
    }

}


fun main(args: Array<String>) {
    val player = Player()
    player.getState().onPlay()
    player.getState().onLock()
    player.getState().onPlay()
    player.getState().onPlay()
    println(player.getState().onNext())

}