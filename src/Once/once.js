const once = (func) =>{
    let canExec = true
    let returnValue
    return function execOnceFunc(){
        if(canExec){
            returnValue = func.apply(this, arguments)
            canExec = false
        }
        return returnValue
    }
}

const fn = () => console.log('one')

const pr = once(fn)
pr()
pr()