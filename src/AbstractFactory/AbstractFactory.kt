package AbstractFactory

interface Furniture

class ArmchairFurniture : Furniture

class SofaFurniture : Furniture


abstract class FurnitureFactory {
    abstract fun createFurniture() : Furniture

    companion object {
        inline fun <reified T : Furniture> createFactory(): FurnitureFactory = when(T::class){
            ArmchairFurniture::class -> ArmchairFactory()
            SofaFurniture::class     -> SofaFactory()
            else                     -> throw IllegalArgumentException()
        }
    }
}



class ArmchairFactory : FurnitureFactory(){
    override fun createFurniture(): Furniture = ArmchairFurniture()
}

class SofaFactory : FurnitureFactory(){
    override fun createFurniture() : Furniture = SofaFurniture()
}

fun main(args: Array<String>) {
    val furnitureFactory = FurnitureFactory.createFactory<ArmchairFurniture>()
    val furniture = furnitureFactory.createFurniture()
    val furnitureFactorySecond = FurnitureFactory.createFactory<SofaFurniture>()
    val sofa = furnitureFactorySecond.createFurniture()
    println("${sofa} ${furniture}")
}