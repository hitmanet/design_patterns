package Singleton

class Singletone{
    companion object {
        private var instance : Singletone? = null
        private fun createInstance(){
            if (instance == null){
                instance = Singletone()
            }
        }
         fun getInstance() : Singletone?{
            if (instance == null) createInstance()
            return instance
        }
    }
}

fun main(args: Array<String>) {
    val singletone = Singletone.getInstance()
}