const after = (func, count) =>{
    let runCount = 0
    return function runAfter(){
        runCount+=1
        if (runCount >= count){
            return func.apply(this, arguments)
        }
    }
}

const fn = (a,b) => console.log(a+b)

const sumAfterTwoCalls = after(fn.bind(this, 2,3), 2)

setTimeout(() =>{
    console.log("1st call has finished")
    sumAfterTwoCalls()
}, 3000)

setTimeout(() =>{
    console.log("2nd call has finished")
    sumAfterTwoCalls()
}, 4000)