package Decorator

interface CoffeeMachine{
    fun makeCoffee()
}


open class BasicCoffeeMachine : CoffeeMachine{
    override fun makeCoffee() = println("Make simple coffee")
}


class TopCoffeeMachine(private val coffeeMachine: CoffeeMachine) : BasicCoffeeMachine() {
    override fun makeCoffee() {
         coffeeMachine.makeCoffee()
         println("add Penka")
    }
}


fun main(args: Array<String>) {
    val coffeeMachine = TopCoffeeMachine(BasicCoffeeMachine())
    coffeeMachine.makeCoffee()
}