package Adapter

class RoundHole(private var radius : Double){
    private fun getRadius() : Double = radius
    fun fits(peg : RoundPeg) : Boolean =(getRadius() >= peg.getRadius())
}


open class RoundPeg(private var radius : Double){
    open fun getRadius() : Double = radius
}


class SquarePeg(private var width : Double){
    fun getWidth() : Double = width
    fun getSquare() : Double = Math.pow(width, 2.0)
}

class SquarePegAdapter(private var peg : SquarePeg) : RoundPeg(peg.getWidth()) {
    override fun getRadius() : Double = Math.sqrt(Math.pow((peg.getWidth()/2), 2.0)*2)
}

fun main(args: Array<String>) {
    val hole = RoundHole(10.0)
    val rpeg = RoundPeg(10.0)
    if (hole.fits(rpeg)){
        println("Round peg fits round hole")
    }
    val smallSqPeg = SquarePeg(2.0)
    val largeSqPeg = SquarePeg(20.0)
    val smallSqPegAdapter = SquarePegAdapter(smallSqPeg)
    val largeSqlPegAdapter = SquarePegAdapter(largeSqPeg)
    if (hole.fits((smallSqPegAdapter))){
        println("Square peg fits round hole")
    }
}