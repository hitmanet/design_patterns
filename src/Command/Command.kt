package Command

data class User(val username: String)

interface Command{
    fun execute()
}

class RemoveUser(val r: Receiver, val user: User) : Command{
    override fun execute() {
        r.deleteUser(user.username)
    }
}

class EditUser(val r: Receiver, val user : User) : Command{
    override fun execute() {
        r.editUser(user.username)
    }
}

class AddUser (val r : Receiver, val user : User) : Command{
    override fun execute() {
        r.saveUser(user.username)
    }
}

class Receiver{
     fun saveUser(username: String) = println("saveUser ${username}")
     fun deleteUser(username: String) = println("delete user ${username}")
     fun editUser(username: String) = println("edit user ${username}")
}

class Invoker{
    fun sendCommand(command: Command){
        command.execute()
    }
}

fun main(args: Array<String>) {
    val r = Receiver()
    val i = Invoker()
    val u = User("petya")
    val addCommand = AddUser(r, u)
    i.sendCommand(addCommand)

}

