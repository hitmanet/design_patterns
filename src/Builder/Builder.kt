package Builder

class Product private constructor(
        val title: String,
        val type: String?,
        val price: Int?
        ) {
    class Builder(val title: String) {
        var type: String? = null
        var price: Int? = null

        fun setType(type: String?): Builder {
            this.type = type
            return this
        }
        fun setPrice(price: Int?): Builder{
            this.price = price
            return this
        }
        fun build() = Product(title, type, price)
    }
}

fun main(args: Array<String>) {
    val banana = Product.Builder("Banana").setType("Fruit").build()
    val happyBox = Product.Builder("HappyBox").setType("Box").setPrice(1999).build()
    println(happyBox.title)
    println(banana.type)
}